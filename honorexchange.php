<?php
/*
PRE-WARNING TO ANYONE WHO KNOWS WHAT THEY'RE DOING WITH CODE.
I've documented this targetted at newbie devs. Minus the $redirect, ignore the rest of it ;)
- MTG
*/
include(__DIR__ . '/globals.php');
if(!function_exists('error')) {
	function error($msg) {
		global $h;
		echo "<div class='error'><h3>Error</h3>" . $msg . "</div>";
		exit($h->endpage());
	}
}
$_GET['spend']  = isset($_GET['spend']) && ctype_alnum($_GET['spend']) ? strtolower(trim($_GET['spend'])) : null;
$_POST['honor'] = isset($_POST['honor']) && ctype_digit(str_replace(',', '', $_POST['honor'])) ? str_replace(',', '', $_POST['honor']) : null;
$userstats      = ['strength', 'agility', 'guard', 'labour', 'iq']; // Must contain a lowercase list of your userstats table structure - couldn't be arsed making the code do it
/* Here's where things can get a little confusing.
Each first-level element has a second-level array attached, and each second has a third!
The first-level element is the "spend" value.
The second-level element is just so the code knows what we're handling.
The third-level is the cost (in honor points) and trade (as actual value of what they get. Example (using default) Trading 1 point can give a user 100 IQ)

Add/edit/delete, at will, the first-level "what"s. Use the defaults as a guide if you're unsure
The same goes for the second-level array values.
The third-level *MUST* always contain "cost" and "trade" (unless you edit the code to accept more arguments, etc.)
*/
$allowed        = [
	'iq' => [
		'first' => [
			'cost' => 1,
			'trade' => 100
		]
	],
	'labour' => [
		'first' => [
			'cost' => 1,
			'trade' => 3000
		]
	],
	'guard' => [
		'first' => [
			'cost' => 1,
			'trade' => 3000
		]
	],
	'agility' => [
		'first' => [
			'cost' => 1,
			'trade' => 3000
		]
	],
	'strength' => [
		'first' => [
			'cost' => 1,
			'trade' => 3000
		]
	],
	'money' => [
		'first' => [
			'cost' => 1,
			'trade' => 2000000
		]
	]
];
if(!empty($_GET['spend'])) {
	if(!in_array($_GET['spend'], $allowed))
		error("You didn't select a valid option");
	$_GET['which'] = isset($_GET['which']) && is_string($_GET['which']) ? $_GET['which'] : null;
	if(empty($allowed[$_GET['spend']][$_GET['which']]))
		error("You didn't select a valid option");
	$cost  = $allowed[$_GET['spend']][$_GET['which']]['cost'];
	$trade = $allowed[$_GET['spend']][$_GET['which']]['trade'];
	if($_GET['spend'] == 'iq')
		$_GET['spend'] = strtoupper($_GET['spend']);
	if(!empty($_POST['honor'])) {
		$fullCost = $_POST['honor'] * $cost;
		if($full > $ir['honor'])
			error("You don't have enough honor for that");
		$fullTrade = $_POST['honor'] * $trade;
		$db->query("UPDATE `users" . (in_array(strtolower($_GET['spend']), $userstats) ? 'tats' : '') . "` SET `" . $_GET['spend'] . " = " . $_GET['spend'] . " + " . $fullTrade . ", `honor` = `honor` - " . $fullCost . " WHERE `userid` = " . $ir['userid']);
		printf("You've traded %s honor for %s %s", number_format($_POST['honor']), $fullTrade, $_GET['spend']);
	} else {
		?><h4>Trading your honor for <?php echo $_GET['spend'];?></h4>
		You have <strong><?php echo number_format($ir['honor']);?></strong> available to spend.<br />
		<form action='honorexchange.php?spend=<?php echo $_GET['spend'];?>&amp;which=<?php echo $_GET['which'];?>' method='post'>
			<table class='table' width='75%'>
				<tr>
					<th width='25%'>Amount</th>
					<td width='75%'><input type='text' name='honor' value='<?php echo number_format($ir['honor']);?>' /></td>
				</tr>
				<tr>
					<td colspan='2' class='center'><input type='submit' name='submit' value='Trade Honor for <?php echo $_GET['spend'];?>' /></td>
				</tr>
			</table>
		</form><?php
	}
}
?>Welcome to the Honor Award Exchange Center!<br />
You have <strong><?php echo number_format($ir['honor']);?></strong> Honor Awards.<br />
What would you like to spend your Honor Awards on?<br /><br />
<a href='honorexchange.php?spend=iq&amp;which=first'>IQ - 100 IQ per Honor Award</a><br />
<a href='honorexchange.php?spend=labour&amp;which=first'>Trade for Labour Points - 3,000 Labour Points per Honor Award</a><br />
<a href='honorexchange.php?spend=guard&amp;which=first'>Trade for Defense - 3,000 Defense per Honor Award</a><br />
<a href='honorexchange.php?spend=agility&amp;which=first'>Trade for Agility - 3,000 Speed per Honor Award</a><br />
<a href='honorexchange.php?spend=strength&amp;which=first'>Trade for Strength - 3,000 strength per Honor Award</a><br />
<a href='honorexchange.php?spend=money&amp;which=first'>Money - $2,000,000 per Honor Award</a><?php
$h->endpage();