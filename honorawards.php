<?php
/*
PRE-WARNING TO ANYONE WHO KNOWS WHAT THEY'RE DOING WITH CODE.
I've documented this targetted at newbie devs. Minus the $redirect, ignore the rest of it ;)
- MTG
*/
$redirect = true; // Set to true to direct users back to awardIndex() upon error. Set to false if you want the clean error message to show
/*
Here's where things can get a little confusing.
Each first-level element has a second-level array attached.
The first-level element is the "what" value.
The second-level array contains which "which"'s are valid - yeah.. Made more sense in my head.

Add/edit/delete, at will, the second-level array values. Whilst they don't need to be in any form of alphanumeric order, I've place these in numeric order
The same goes for the first-level "what"s. Use the defaults as a guide if you're unsure
*/
$allowed  = [
	'level' => [50, 100, 200, 300, 400, 500],
	'crimes' => [1000, 5000, 10000, 20000, 30000],
	'crystals' => [2000, 5000, 10000, 20000, 50000],
	'money' => [5000000, 2000000000, 10000000000, 60000000000, 130000000000, 1000000000000, 25000000000000, 60000000000000],
	'daysingang' => [100, 200, 300, 400, 500],
	'donatordays' => [150, 200, 300, 400, 500]
];
require_once(__DIR__ . '/globals.php');
if(!function_exists('error')) {
	function error($msg) {
		global $h, $redirect;
		if($redirect) {
			awardIndex();
			exit($h->endpage());
		} else {
			echo "<div class='error'><h3>Error</h3>" . $msg . "</div>";
			exit($h->endpage());
		}
	}
}
?><h3>Honor Awards</h3><?php
$_GET['action'] = isset($_GET['action']) && ctype_alpha($_GET['action']) ? strtolower(trim($_GET['action'])) : null;
switch($_GET['action']) {
	case 'check':
		checkAward();
		break;
	default:
		awardIndex();
		break;
}
function awardIndex() {
	?><p><span style='color:tan;'>Level Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=50'>Level 50 Honor Award</a><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=100'>Level 100 Honor Award</a><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=200'>Level 200 Honor Award</a><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=300'>Level 300 Honor Award</a><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=400'>Level 400 Honor Award</a><br />
	<a href='honorawards.php?action=check&amp;what=level&amp;which=500'>Level 500 Honor Award</a></p>

	<p><span style='color:tan;'>Crime Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=crimes&amp;which=1000'>1000 crimes completed</a><br />
	<a href='honorawards.php?action=check&amp;what=crimes&amp;which=5000'>5000 crimes completed</a><br />
	<a href='honorawards.php?action=check&amp;what=crimes&amp;which=10000'>10000 crimes completed</a><br />
	<a href='honorawards.php?action=check&amp;what=crimes&amp;which=20000'>20000 crimes completed</a><br />
	<a href='honorawards.php?action=check&amp;what=crimes&amp;which=30000'>30000 crimes completed</a></p>

	<p><span style='color:tan;'>Diamond Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=crystals&amp;which=2000'>2000 crystals</a><br />
	<a href='honorawards.php?action=check&amp;what=crystals&amp;which=5000'>5000 crystals</a><br />
	<a href='honorawards.php?action=check&amp;what=crystals&amp;which=10000'>10000 crystals</a><br />
	<a href='honorawards.php?action=check&amp;what=crystals&amp;which=20000'>20000 crystals</a><br />
	<a href='honorawards.php?action=check&amp;what=crystals&amp;which=50000'>50000 crystals</a></p>

	<p><span style='color:tan;'>Money Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=5000000'>500 million cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=2000000000'>2 billion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=10000000000'>10 billion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=60000000000'>60 billion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=130000000000'>130 billion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=1000000000000'>1 trillion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=25000000000000'>25 trillion cash</a><br />
	<a href='honorawards.php?action=check&amp;what=money&amp;which=60000000000000'>60 trillion cash</a></p>

	<p><span style='color:tan;'>Gang Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=daysingang&amp;which=100'>100 Days in Gang</a><br />
	<a href='honorawards.php?action=check&amp;what=daysingang&amp;which=200'>200 Days in Gang</a><br />
	<a href='honorawards.php?action=check&amp;what=daysingang&amp;which=300'>300 Days in Gang</a><br />
	<a href='honorawards.php?action=check&amp;what=daysingang&amp;which=400'>400 Days in Gang</a><br />
	<a href='honorawards.php?action=check&amp;what=daysingang&amp;which=500'>500 Days in Gang</a></p>

	<p><span style='color:tan;'>Donator Awards</span><br />
	<a href='honorawards.php?action=check&amp;what=donatordays&amp;which=150'>150 Donator Days</a><br />
	<a href='honorawards.php?action=check&amp;what=donatordays&amp;which=200'>200 Donator Days</a><br />
	<a href='honorawards.php?action=check&amp;what=donatordays&amp;which=300'>300 Donator Days</a><br />
	<a href='honorawards.php?action=check&amp;what=donatordays&amp;which=400'>400 Donator Days</a><br />
	<a href='honorawards.php?action=check&amp;what=donatordays&amp;which=500'>500 Donator Days</a></p><?php
}
function checkAward() {
	global $db, $ir, $allowed;
	$_GET['what'] = isset($_GET['what']) && in_array($_GET['what'], $allowed) ? strtolower(trim($_GET['what'])) : null;
	if(empty($_GET['what']))
		error("You didn't select a valid option");
	$_GET['which'] = isset($_GET['which']) && in_array($_GET['which'], $allowed[$_GET['what']]) ? trim($_GET['which']) : null;
	if(empty($_GET['which']))
		error("You didn't select a valid option");
	$check = $db->query("SELECT * FROM `honorawards` WHERE `list` = '" . $_GET['what'] . ":" . $_GET['which'] . "' AND `userid` = " . $ir['userid']);
	if($db->num_rows($check))
		error("You've already received that award");
	$db->query("INSERT INTO `honorawards` (`userid`, `list`) VALUES (" . $ir['userid'] . ", '" . $_GET['what'] . ":" . $_GET['which'] . "')");
	$db->query("UPDATE `users` SET `honor` = `honor` + 1 WHERE `userid` = " . $ir['userid']);
	$_GET['what'] = str_replace(['daysingang', 'donatordays'], ['days in gang', 'donator days'], $_GET['what']);?>You've claimed the honor of <?php echo ucwords($_GET['what']);?>: <?php echo format($_GET['which']);
}
$h->endpage();